package org.bp.hotelui;

import org.bp.ui.MedicalUiApplication;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = MedicalUiApplication.class)
class HotelUiApplicationTests {

	@Test
	void contextLoads() {
	}

}
