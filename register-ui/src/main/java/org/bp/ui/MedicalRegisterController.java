package org.bp.ui;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.namespace.QName;
import javax.xml.transform.Source;

import org.bp.medical.Drug;
import org.bp.medical.GetPrescriptionResponse;
import org.bp.medical.Id;
import org.bp.medical.MedicalRequest;
import org.bp.medical.MedicalResponse;
import org.bp.medical.Patient;
import org.bp.medical.Prescription;
import org.bp.medical.RegisterPrescriptionResponse;
import org.bp.medical.VisitResponse;
import org.hibernate.validator.internal.util.logging.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;
import org.slf4j.Logger;


@Controller
public class MedicalRegisterController {

	
	@GetMapping("/choose_prescription")
	public String getPrescriptionForm(Model model) {
	
		
		model.addAttribute("Id","0");
		return "choose_prescription";
	}
	
	
	
	@PostMapping("/choose_prescription")
	public String getPrescription(@ModelAttribute("Id") String Id, Model model) {
     
		RestTemplate restTemplate = new RestTemplate();
		try {
			String uri = "http://localhost:8085/api/medical/prescription/" + Id;
			ResponseEntity<String> re = restTemplate.getForEntity(
					uri , String.class);
			
			String registerPrescriptionResponse = re.getBody();
			String[] list = registerPrescriptionResponse.split("\\|\\|");
			Prescription prescription = new Prescription();
			Patient patient = new Patient();
			
			prescription.setExtraDescription(list[0]);
			patient.setAge(Integer.valueOf(list[1]));
			patient.setFirstName(list[2]);
			patient.setLastName(list[3]);
			patient.setGender(list[4]);
			prescription.setId(Integer.valueOf(list[5]));
			prescription.setPatient(patient);
			for(int i=6;i<list.length;i++) {
				Drug drug = new Drug();
				String[] listofDrugs = list[6].split("%%");
				drug.setDosage(listofDrugs[0]);
				drug.setName(listofDrugs[1]);
				drug.setQuantity(Integer.valueOf(listofDrugs[2]));
				prescription.addDrugsItem(drug);
			}
			
			model.addAttribute("visitinfo", prescription);
			return "result_prescription";
		} 
		catch(Exception e){
			model.addAttribute("error_msg_get", "Nie istnieje wizyta o podanym ID");
			return "error_get";
		}
        	
	}

	
	@GetMapping("/choose_visit")
	public String getVisitForm(Model model) {
	
		model.addAttribute("visit_id", "0");
		return "choose_visit";
	}
	
	@PostMapping("/choose_visit")
	public String getVisit(@ModelAttribute("visit_id") String visit_id, Model model) {
     
		RestTemplate restTemplate = new RestTemplate();
		try {
			ResponseEntity<VisitResponse> re = restTemplate.getForEntity(
					"http://localhost:8085/api/medical/getVisit/" + visit_id, VisitResponse.class);
			
			VisitResponse visitResponse = re.getBody();
			model.addAttribute("visitResponse", visitResponse);
			return "result_visit";
		} 
		catch(Exception e){
			model.addAttribute("error_msg_get", "Nie istnieje wizyta o podanym ID");
			return "error_get";
		}
        	
	}
	
	
	
	@GetMapping("/index")
	public String index(Model model) {
		MedicalRequest medicalRequest = new MedicalRequest();

		return "index";
	}
	
	@GetMapping("/register")
	public String bookRoomForm(Model model) {
		MedicalRequest medicalRequest = new MedicalRequest();
	
		model.addAttribute("medicalRequest", medicalRequest);
		return "register";
	}
	
	@PostMapping("/register")
	public String register(@ModelAttribute MedicalRequest medicalRequest, Model model) {
     
		
		if(medicalRequest.getPatient().getAge() <= 0) {
			model.addAttribute("error_msg","Podano nieprawidłowy wiek pacjenta");
			model.addAttribute("error_msg2","Rejestracja obu usług zakończona niepowodzeniem");
			return "error";
		}
		RestTemplate restTemplate = new RestTemplate();
		try {
			ResponseEntity<MedicalResponse> re = restTemplate.postForEntity(
					"http://localhost:8085/api/medical/visit", medicalRequest, MedicalResponse.class);

			MedicalResponse medicalResponse = re.getBody();
			model.addAttribute("visitinfo", medicalResponse);
			return "result";
		}
		catch(HttpStatusCodeException e) {
	        ResponseEntity<String> tmp = ResponseEntity.status(e.getRawStatusCode()).headers(e.getResponseHeaders()).body(e.getResponseBodyAsString());
	        String body = tmp.getBody();
	        if(body.contains("http://localhost:8083/visit")) {
	        	model.addAttribute("error_msg","Nie można leczyć samego siebie");
	        	model.addAttribute("error_msg2","Rejestracja wizyty zakończona niepowodzeniem");
	        	model.addAttribute("error_msg3","Rejestracja recepty została wycofana");
	        	return "error";
	        }
	        else if(body.contains("SoapFaultClientException")) {
	        	model.addAttribute("error_msg","Podano nieprawidłową ilość leku");
	        	model.addAttribute("error_msg2","Rejestracja recepty zakończona niepowodzeniem");
	        	model.addAttribute("error_msg3","Rejestracja wizyty została wycofana");
	        	return "error";
	        }
		}
		return "error";
        	
	}


}
