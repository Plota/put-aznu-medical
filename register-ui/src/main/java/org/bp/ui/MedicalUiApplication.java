package org.bp.ui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedicalUiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MedicalUiApplication.class, args);
	}

}
