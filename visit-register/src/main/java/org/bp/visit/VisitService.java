package org.bp.visit;

import java.util.HashMap;

import org.bp.visit.model.VisitException;
import org.bp.visit.model.VisitRequest;
import org.bp.visit.model.VisitResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@org.springframework.web.bind.annotation.RestController

@OpenAPIDefinition(info = @Info(
        title = "Visit service",
        version = "1",
        description = "Service for medical visit register"))

public class VisitService {
	
		HashMap<Integer, VisitRequest> map = new HashMap<>();
		Integer globalId = 0;
		
		
		
		@GetMapping("/visit/{id}")
		public @ResponseBody VisitResponse getById(@PathVariable Integer id) {
			
			if(map.containsKey(id)) {
				VisitRequest visitRequest = map.get(id);
				VisitResponse visitResponse = new VisitResponse(globalId, visitRequest);
				return visitResponse;
			}
			else {
				throw new VisitException("Nie ma recepty o takim Id"); 
			}
			
		    
		}
		
		
		
		@org.springframework.web.bind.annotation.PostMapping("/visit")
	    @Operation(
	            summary = "Register visit",
	            description = "Register your medical visit",
	            responses = {
	                @ApiResponse(responseCode = "200",
	                        description = "OK",
	                        content = {@Content(mediaType = "application/json", schema = @Schema(implementation = VisitResponse.class))}),
	                @ApiResponse(responseCode = "400", description = "Bad Request",
	                        content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ExceptionResponse.class))})
	            })		
		
		public VisitResponse visit(
				@org.springframework.web.bind.annotation.RequestBody VisitRequest visitRequest) {
		
			if(!visitRequest.hasPatient()) {
				throw new VisitException("Nie podano pacjenta");
			}
			if(visitRequest.hasPatient() && visitRequest.getPatient().getAge() <= 0) {
				throw new VisitException("Wiek musi byc wiekszy niz 0");
			}
			if(!visitRequest.hasDoctor()) {
				throw new VisitException("Nie podano doktora");
			}
			if(!visitRequest.hasDiagnosis()) {
				throw new VisitException("Nie podano diagnozy");
			}
			if(visitRequest.getDoctor().getFirstName().equals(visitRequest.getPatient().getFirstName()) && 
			   visitRequest.getDoctor().getLastName().equals(visitRequest.getPatient().getLastName())) {
				throw new VisitException("Nie mozna diagnozować samemu siebie!");
			}
			
			globalId++;
			map.put(globalId, visitRequest);
			
			VisitResponse visitResponse = new VisitResponse(globalId, visitRequest);
			return visitResponse;
		}

}
