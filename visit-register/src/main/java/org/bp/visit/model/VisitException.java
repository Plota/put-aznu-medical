package org.bp.visit.model;

public class VisitException extends RuntimeException{

	
	public VisitException(String msg) {
		super(msg);
	}

}
