package org.bp.visit.model;

public class VisitRequest {
	protected Patient patient;
	protected Doctor doctor;
	protected Diagnosis diagnosis;
	
	public boolean hasPatient() {
		if(this.patient == null) {
			return false;
		}
		else {
			
			if(this.patient.getFirstName() 	!= null && this.patient.getFirstName() 	!= "" &&
			   this.patient.getLastName() 	!= null && this.patient.getLastName() 	!= "" &&
			   this.patient.getGender() 	!= null && this.patient.getGender() 	!= "" ){
				
				return true;
			}
			else
				return false;
				
		}
	}
	
	public boolean hasDoctor() {
		if(this.doctor == null) {
			return false;
		}
		else {
			if(this.doctor.getFirstName() 		!= null && this.doctor.getFirstName() 		!= "" &&
			   this.doctor.getLastName() 		!= null && this.doctor.getLastName() 		!= "" &&
			   this.doctor.getSpecialization() 	!= null && this.doctor.getSpecialization() 	!= "") {
				
				return true;
			} 
			else {
				return false;
			}
				
		}
		
	}
	
	
	public boolean hasDiagnosis() {
		if(this.diagnosis == null) {
			return false;
		}
		else {
			if(this.diagnosis.getDisease() 			!= null && this.diagnosis.getDisease() 			!= "" &&
			   this.diagnosis.getPatientsState() 	!= null && this.diagnosis.getPatientsState() 	!= "") {
				return true;
			}
			else {
				return false;
			}
			
		}
	}
	
	
	
	public Patient getPatient() {
		return patient;
	}
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	public Doctor getDoctor() {
		return doctor;
	}
	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}
	public Diagnosis getDiagnosis() {
		return diagnosis;
	}
	public void setDiagnosis(Diagnosis diagnosis) {
		this.diagnosis = diagnosis;
	}
	
}
