package org.bp.visit.model;

public class Diagnosis {
	protected String patientsState;
	protected String disease;
	public String getPatientsState() {
		return patientsState;
	}
	public void setPatientsState(String patientsState) {
		this.patientsState = patientsState;
	}
	public String getDisease() {
		return disease;
	}
	public void setDisease(String disease) {
		this.disease = disease;
	}
	
	
}
