package org.bp.visit.model;


public class VisitResponse {
	
	
	private int visitId;
	private VisitRequest visitRequest;
	
	public VisitResponse (int visitId, VisitRequest visitRequest ) {
		this.visitId = visitId;
		this.visitRequest = visitRequest;
	}
	
	public int getVisitId() {
		return visitId;
	}
	public void setVisitId(int visitId) {
		this.visitId = visitId;
	}
	public VisitRequest getVisitRequest() {
		return visitRequest;
	}
	public void setVisitRequest(VisitRequest visitRequest) {
		this.visitRequest = visitRequest;
	}
	
	

	
}
