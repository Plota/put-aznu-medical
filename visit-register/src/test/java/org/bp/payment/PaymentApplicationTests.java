package org.bp.payment;

import org.bp.visit.VisitApplication;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = VisitApplication.class)
class PaymentApplicationTests {

	@Test
	void contextLoads() {
	}

}
