package org.bp.travel;

import org.bp.medical.MedicalApplication;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = MedicalApplication.class)
class TravelBookingApplicationTests {

	@Test
	void contextLoads() {
	}

}
