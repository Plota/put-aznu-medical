package org.bp.medical;

import static org.apache.camel.model.rest.RestParamType.body;

import java.util.LinkedHashMap;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.converter.jaxb.JaxbDataFormat;
import org.apache.camel.model.SagaPropagation;
import org.apache.camel.model.rest.RestBindingMode;
import org.apache.camel.model.rest.RestParamType;
import org.bp.medical.model.MedicalRequest;
import org.bp.medical.model.MedicalResponse;
import org.bp.medical.model.Utils;
import org.bp.prescription.CancelPrescription;
import org.bp.prescription.Drug;
import org.bp.prescription.GetPrescription;
import org.bp.prescription.GetPrescriptionResponse;
import org.bp.prescription.Prescription;
import org.bp.prescription.RegisterPrescriptionResponse;
import org.bp.visit.Diagnosis;
import org.bp.visit.Doctor;
import org.bp.visit.Patient;
import org.bp.visit.VisitRequest;
import org.bp.visit.VisitResponse;
import org.springframework.stereotype.Component;

@Component
public class medicalService  extends RouteBuilder{

	@org.springframework.beans.factory.annotation.Autowired
	MedicalIdentifierService medicalIdentifierService;
	
	@Override
	public void configure() throws Exception {
		
		
		final JaxbDataFormat jaxbPrescription = new JaxbDataFormat(RegisterPrescriptionResponse.class.getPackage().getName());
		final JaxbDataFormat jaxbgetPrescription = new JaxbDataFormat(GetPrescriptionResponse.class.getPackage().getName());
		final JaxbDataFormat jaxbVisit = new JaxbDataFormat(VisitResponse.class.getPackage().getName());
		
		
		//--------------------------REST CONFIG--------------------------------------------------------------------------//
		restConfiguration()
        .component("servlet")
        .bindingMode(RestBindingMode.json)
        .dataFormatProperty("prettyPrint", "true")
        .enableCORS(true)
        .contextPath("/api")
        // turn on swagger api-doc
        .apiContextPath("/api-doc")
        .apiProperty("api.title", "medical visit API")
        .apiProperty("api.version", "1.0.0");
        

        //--------------------------Post REST--------------------------------------------------------------------------//
        
		
        rest("/medical").description("medical visit service")
        .consumes("application/json")
        .produces("application/json")        
        .post("/visit").description("Create an medical visit entry").type(MedicalRequest.class).outType(MedicalResponse.class)
        .param().name("body").type(body).description("Detail of medical visit and prescription").endParam()
        .responseMessage().code(200).message("medical visit successfully registered").endResponseMessage()
        .to("direct:medical")
        ;  
        

        //--------------------------Get Prescription REST--------------------------------------------------------------------------//
        
        rest("/medical").description("medical visit service")
        .consumes("application/json")
        .produces("application/json")  
        .get("/prescription/{id}").description("Get prescription").type(Integer.class).outType(String.class)
        .param().name("id")
        .description("Customer ID")
        .type(RestParamType.path)
        .dataType("int").endParam()
        .responseMessage().code(200).message("prescription successfully getted").endResponseMessage()
        .to("direct:medical2");
        
        //--------------------------Get Visit REST--------------------------------------------------------------------------//
        
        
        rest("/medical").description("medical visit service")
        .consumes("application/json")
        .produces("application/json")  
        .get("/getVisit/{id}").description("Get visit").type(Integer.class).outType(VisitResponse.class)
        .param().name("id")
        .description("Visit ID")
        .type(RestParamType.path)
        .dataType("int").endParam()
        .responseMessage().code(200).message("medical visit successfully getted").endResponseMessage()
        .to("direct:medical3"); 
        
        

        
      //--------------------------Post Prescription-------------------------------------------------------------------//

        from("direct:registerPrescription").routeId("registerPrescription")
        .log("visit fired")
        .saga()
    	.propagation(SagaPropagation.MANDATORY)
    	.compensation("direct:cancelPrescription").option("medicalId", simple("${exchangeProperty.medicalId}"))        
        .process((exchange) -> 
        	{exchange.getMessage().setBody(
        			Utils.prepareRegisterPrescription(exchange.getMessage().getBody(MedicalRequest.class))); 
        			} )
        .marshal(jaxbPrescription)
        .to("spring-ws:http://localhost:8081/soap-api/service/prescription")
        .to("stream:out")
        .unmarshal(jaxbPrescription)
		.process((exchange) -> {
			RegisterPrescriptionResponse registerPrescriptionResponse = exchange.getMessage().getBody(RegisterPrescriptionResponse.class);
			exchange.setProperty("registerPrescriptionResponse", registerPrescriptionResponse);
			String medicalId=exchange.getProperty("medicalId", String.class);
			medicalIdentifierService.assignPrescriptionId(medicalId, registerPrescriptionResponse.getReturn().getId());
		})        
        ;
        
      //--------------------------Cancel Prescription--------------------------------------------------------------------------//
        
        from("direct:cancelPrescription").routeId("cancelPrescription")
        .log("cancelPrescription fired")
        .process((exchange) -> {
        	
        	String medicalId = exchange.getMessage().getHeader("medicalId", String.class);
        	int prescriptionId=medicalIdentifierService.getPrescriptionId(medicalId);
        	CancelPrescription cancelPrescription = new CancelPrescription();
        	cancelPrescription.setArg0(prescriptionId);
        	exchange.getMessage().setBody(cancelPrescription); 
        	} )
        .marshal(jaxbPrescription)
        .to("spring-ws:http://localhost:8081/soap-api/service/prescription")
        .to("stream:out")
        .unmarshal(jaxbPrescription)        
        ;
        
      //--------------------------Get Prescription--------------------------------------------------------------------------//
        
        from("direct:getPrescription").routeId("getPrescription")
        .log("getPrescription fired")
        .process((exchange) -> {
        	
        	GetPrescription getPrescription = new GetPrescription();
        	
        	int id = Integer.valueOf(exchange.getIn().getHeader("id").toString());
        	getPrescription.setArg0(id);
  
        	exchange.getMessage().setBody(getPrescription); 
        	} )
        .marshal(jaxbgetPrescription)
        .to("spring-ws:http://localhost:8081/soap-api/service/prescription")
        .to("stream:out")
        .unmarshal(jaxbgetPrescription)        
        ;
       
        

     

        
      //--------------------------Visit POST REST--------------------------------------------------------------------------//
        
        from("direct:visit").routeId("visit")
        .log("visit fired")
        .marshal().json()
        .removeHeaders("CamelHttp*")
        .to("rest:post:visit?host=localhost:8083")
        .unmarshal().json()
      
        ;
        
 
        
    //--------------------------Visit Get REST--------------------------------------------------------------------------//
        
        from("direct:getVisit").routeId("getVisit")
        .log("get visit fired")
        .marshal().json()
        .removeHeaders("CamelHttp*")
        .to("rest:get:visit/{id}?host=localhost:8083")
        .unmarshal().json()
      
        ;
        
 

      //--------------------------ENDPOINT--------------------------------------------------------------------------//
        
        from("direct:medical").routeId("medical")
        .log("medical fired")
        .process(
	        		(exchange) -> {					
	        			exchange.setProperty("visitRequest", 
	        					Utils.prepareVisitRequest(exchange.getMessage().getBody(MedicalRequest.class)));
	        			exchange.setProperty("medicalId", medicalIdentifierService.generateMedicalId()); 
	        		}
        		)
        .saga()
        .multicast()
        .parallelProcessing()
        .aggregationStrategy(
        		(prevEx, currentEx) -> {
        			if (currentEx.getException()!=null)
        				return currentEx;
        			if (prevEx!=null && prevEx.getException()!=null)
        				return prevEx;
        			
    
        			return currentEx;
        			}
        		)
        .to("direct:registerPrescription")
        .end()
        .process(
        		(currentEx) -> {
        			currentEx.getMessage().setBody(currentEx.getProperty("visitRequest", VisitRequest.class));		
        		}
        		)
        .to("direct:visit")
        .process(
        		(currentEx) -> {
        			MedicalResponse medicalResponse = new MedicalResponse();
        			medicalResponse.setMedicalid(currentEx.getProperty("medicalId",String.class));
        			medicalResponse.setRegisterPrescriptionResponse(currentEx.getProperty("registerPrescriptionResponse", RegisterPrescriptionResponse.class));
        			Object body = currentEx.getIn().getBody();
        		
        			LinkedHashMap map =  (LinkedHashMap) body;
        			
        			int visitId = (int) map.get("visitId");
        			
        			String medicalId= currentEx.getProperty("medicalId", String.class);
        			medicalIdentifierService.assignPrescriptionId(medicalId, visitId);
        			
        			LinkedHashMap visitRequestMap = (LinkedHashMap) map.get("visitRequest");
        			LinkedHashMap patientMap = (LinkedHashMap) visitRequestMap.get("patient");
        			LinkedHashMap doctorMap = (LinkedHashMap) visitRequestMap.get("doctor");
        			LinkedHashMap diagnosisMap = (LinkedHashMap) visitRequestMap.get("diagnosis");
        			
        			Patient patient = new Patient();
        			patient.setAge((int)patientMap.get("age"));
        			patient.setFirstName((String)patientMap.get("firstName"));
        			patient.setLastName((String)patientMap.get("lastName"));
        			patient.setGender((String)patientMap.get("gender"));
        			
        			Doctor doctor = new Doctor();
        			doctor.setFirstName((String)doctorMap.get("firstName"));
        			doctor.setLastName((String)doctorMap.get("lastName"));
        			doctor.setSpecialization((String)doctorMap.get("specialization"));
        			
        			Diagnosis diagnosis = new Diagnosis();
        			diagnosis.setDisease((String)diagnosisMap.get("disease"));
        			diagnosis.setPatientsState((String)diagnosisMap.get("patientsState"));
        			
        			VisitRequest visitRequest = new VisitRequest();
        			visitRequest.setDiagnosis(diagnosis);
        			visitRequest.setDoctor(doctor);
        			visitRequest.setPatient(patient);
        			
        			VisitResponse visitResponse = new VisitResponse();
        			visitResponse.setVisitId(visitId);
        			visitResponse.setVisitRequest(visitRequest);
        			
        			medicalResponse.setVisitResponse(visitResponse);
        			
        			currentEx.getMessage().setBody(medicalResponse);
        		}
        		)
        .removeHeaders("Camel*")
        .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(200))
        //.convertBodyTo(String.class)
        ;      
        

        
        //--------------------------Get Prescription FINAL--------------------------------------------------------------------------//
        
        
        from("direct:medical2").routeId("medical2")
        .log("medical fired")
        .process((exchange) -> {})
        .to("direct:getPrescription")
        .process((exchange) -> {
        	
        	GetPrescriptionResponse getPrescriptionResponse = (GetPrescriptionResponse) exchange.getIn().getBody();
        	Prescription prescription = getPrescriptionResponse.getReturn();
            StringBuilder sb = new StringBuilder();
            
            sb.append(prescription.getExtraDescription());
            sb.append("||").append(prescription.getPatient().getAge());
            sb.append("||").append(prescription.getPatient().getFirstName());
            sb.append("||").append(prescription.getPatient().getLastName());
            sb.append("||").append(prescription.getPatient().getGender());
            sb.append("||").append(prescription.getId());
            for(Drug drug : prescription.getDrugs()) {
            	sb.append("||").append(drug.getDosage()).append("%%");
            	sb.append(drug.getName()).append("%%");
            	sb.append(drug.getQuantity());
            }
            String tmp = sb.toString();
            tmp = tmp.replace("ę", "e");
            tmp = tmp.replace("ą", "ą");
            tmp = tmp.replace("ł", "l");
            tmp = tmp.replace("ó", "o");
            tmp = tmp.replace("ż", "z");
            tmp = tmp.replace("ć", "c");
            tmp = tmp.replace("ń", "n");
        	exchange.getIn().setBody(tmp);
        	System.out.print("ads");
        	} )
        .removeHeaders("Camel*")
        .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(200)); 
        
        //--------------------------Get visit FINAL--------------------------------------------------------------------------//
        
        
        from("direct:medical3").routeId("medical3")
        .log("visit fired")
        .process((exchange) -> {
        })
        .to("direct:getVisit")
        .process((exchange) -> {
        	
        System.out.println("dso"); 
        	} )
        .removeHeaders("Camel*")
        .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(200));

	}

}
