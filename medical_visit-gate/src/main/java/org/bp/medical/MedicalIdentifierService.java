package org.bp.medical;


import java.util.HashMap;
import java.util.UUID;

import org.springframework.stereotype.Service;

@Service
public class MedicalIdentifierService {
HashMap<String, MedicalIds> medicalIdsMap =  new HashMap<>();
	
	public String generateMedicalId() {
		String medicalID=UUID.randomUUID().toString();
		MedicalIds bookingIds= new MedicalIds();
		medicalIdsMap.put(medicalID, bookingIds);
		return medicalID;
	}
	
	public void assignVisitId(String medicalId, int visitId) {
		medicalIdsMap.get(medicalId).setVisitBookingId(visitId);
	}
	
	public void assignPrescriptionId(String medicalId, int prescriptionId) {
		medicalIdsMap.get(medicalId).setPrescriptionId(prescriptionId);
	}
	
	public int getVisitId(String medicalId) {
		return medicalIdsMap.get(medicalId).getVisitId();
	}
	
	public int getPrescriptionId(String medicalId) {
		return medicalIdsMap.get(medicalId).getPrescriptionId();
	}
	
	public static class MedicalIds{
		private int visitId;
		private int prescriptionId;

		public int getVisitId() {
			return visitId;
		}
		public void setVisitBookingId(int visitId) {
			this.visitId = visitId;
		}
		public int getPrescriptionId() {
			return prescriptionId;
		}
		public void setPrescriptionId(int prescriptionId) {
			this.prescriptionId = prescriptionId;
		}

		
	}
}
