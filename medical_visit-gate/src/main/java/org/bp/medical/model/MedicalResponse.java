package org.bp.medical.model;

import org.bp.prescription.RegisterPrescriptionResponse;
import org.bp.visit.VisitResponse;

public class MedicalResponse {
	protected String medicalid;
	protected VisitResponse visitResponse;
	protected RegisterPrescriptionResponse registerPrescriptionResponse;
	
	public String getMedicalid() {
		return medicalid;
	}
	public void setMedicalid(String medicalid) {
		this.medicalid = medicalid;
	}
	public VisitResponse getVisitResponse() {
		return visitResponse;
	}
	public void setVisitResponse(VisitResponse visitResponse) {
		this.visitResponse = visitResponse;
	}
	public RegisterPrescriptionResponse getRegisterPrescriptionResponse() {
		return registerPrescriptionResponse;
	}
	public void setRegisterPrescriptionResponse(RegisterPrescriptionResponse registerPrescriptionResponse) {
		this.registerPrescriptionResponse = registerPrescriptionResponse;
	}
	
	
}
