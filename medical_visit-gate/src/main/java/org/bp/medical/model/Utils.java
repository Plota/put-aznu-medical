package org.bp.medical.model;



import org.bp.prescription.Drug;
import org.bp.prescription.Patient;
import org.bp.prescription.RegisterPrescription;
import org.bp.prescription.RegisterPrescriptionResponse;
import org.bp.visit.Diagnosis;
import org.bp.visit.Doctor;
import org.bp.visit.VisitRequest;
import org.bp.visit.VisitResponse;


public class Utils {

	public static RegisterPrescription prepareRegisterPrescription(MedicalRequest medicalRequest) {
		RegisterPrescription registerPrescription = new RegisterPrescription();
		
		Patient patient = new Patient();
		patient.setAge(medicalRequest.getPatient().getAge());
		patient.setFirstName(medicalRequest.getPatient().getFirstName());
		patient.setLastName(medicalRequest.getPatient().getLastName());
		patient.setGender(medicalRequest.getPatient().getGender());
		registerPrescription.setArg0(patient);
		
		
		if(medicalRequest.drugs != null) {
			for(Drug drug : medicalRequest.getDrugs()) {
				registerPrescription.getArg1().add(drug);
			}
		}
		
		registerPrescription.setArg2(medicalRequest.getExtraDescription());
		return registerPrescription;
	}
	
	public static VisitRequest prepareVisitRequest(MedicalRequest medicalRequest) {
		VisitRequest visitRequest = new VisitRequest();
		
		Doctor doctor = medicalRequest.getDoctor();
		visitRequest.setDoctor(doctor);
		
		org.bp.visit.Patient patient = medicalRequest.getPatient();
		visitRequest.setPatient(patient);
		
		Diagnosis diagnosis = medicalRequest.getDiagnosis();
		visitRequest.setDiagnosis(diagnosis);
		
		return visitRequest;
		
	}
	
	public static MedicalResponse createMedicalResponse(String medicalid, VisitResponse visitResponse, RegisterPrescriptionResponse registerPrescriptionResponse) {
	
		MedicalResponse medicalResponse = new MedicalResponse();

		medicalResponse.setMedicalid(medicalid);
		medicalResponse.setRegisterPrescriptionResponse(registerPrescriptionResponse);
		medicalResponse.setVisitResponse(visitResponse);
		
		return medicalResponse;
	}
}

