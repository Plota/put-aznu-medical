package org.bp.medical.model;

import java.util.ArrayList;

import org.bp.prescription.Drug;
import org.bp.prescription.Prescription;
import org.bp.visit.Diagnosis;
import org.bp.visit.Doctor;
import org.bp.visit.Patient;

public class MedicalRequest {

	private Patient patient;
	private Doctor doctor;
	private Diagnosis diagnosis;
	ArrayList<Drug> drugs;
	private String extraDescription;
	
	
	public ArrayList<Drug> getDrugs() {
		return this.drugs;
	}
	public void setDrugs(ArrayList<Drug> drugs) {
		this.drugs = drugs;
	}
	public String getExtraDescription() {
		return extraDescription;
	}
	public void setExtraDescription(String extraDescription) {
		this.extraDescription = extraDescription;
	}
	public Patient getPatient() {
		return patient;
	}
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	public Doctor getDoctor() {
		return doctor;
	}
	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}
	public Diagnosis getDiagnosis() {
		return diagnosis;
	}
	public void setDiagnosis(Diagnosis diagnosis) {
		this.diagnosis = diagnosis;
	}

	
	
}
