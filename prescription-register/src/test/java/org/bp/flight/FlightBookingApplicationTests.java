package org.bp.flight;

import org.bp.prescription.registerPrescriptionApplication;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = registerPrescriptionApplication.class)
class FlightBookingApplicationTests {

	@Test
	void contextLoads() {
	}

}
