package org.bp.prescription;



import java.util.ArrayList;
import java.util.HashMap;

import org.bp.types.Drug;
import org.bp.types.Fault;
import org.bp.types.Patient;
import org.bp.types.Prescription;

@javax.jws.WebService
@org.springframework.stereotype.Service
public class PrescriptionService {
	
	HashMap<Integer, Prescription> map = new HashMap<>();
	Integer globalId = 0;
	
	public Prescription registerPrescription(Patient patient, ArrayList<Drug> drugs, String extraDescription) throws Fault {

		
		if(patient != null) {
			if(patient.isWrong()) {
				throw new Fault(3,"Nie podano pacjenta");
			}
		}
		else {
			throw new Fault(3,"Nie podano pacjenta");
		}
		
		if(drugs == null) {
			throw new Fault(4,"Nie podano leków");
		}
		else {
			for(Drug drug : drugs) {
				if(drug.isWrong()) {
					throw new Fault(5,"Bledny format lekow");
				}
			}
		}
		
			
		
		globalId++;
		Prescription prescription = new Prescription(globalId,patient,drugs,extraDescription);
		map.put(globalId, prescription);
		return prescription;
	}
	
	
	public Prescription cancelPrescription(int prescriptionId) throws Fault{
		
		
		if(map.containsKey(prescriptionId)) {
			Prescription prescription = map.get(prescriptionId);
			map.remove(prescriptionId);
			globalId--;
			return prescription;	
		}
		else {
			throw new Fault(1,"Recepta o podanym id nie istnieje");
		}
	}

public Prescription getPrescription(int prescriptionId) throws Fault{
		
	if(map.containsKey(prescriptionId)) {
		Prescription prescription = map.get(prescriptionId);

		
		return prescription;	
	}
	else {
		throw new Fault(2,"Recepta o podanym id nie istnieje");
	}
	}
	
}
