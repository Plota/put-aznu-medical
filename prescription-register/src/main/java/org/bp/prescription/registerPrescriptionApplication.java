package org.bp.prescription;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class registerPrescriptionApplication {

	public static void main(String[] args) {
		SpringApplication.run(registerPrescriptionApplication.class, args);
	}

}
