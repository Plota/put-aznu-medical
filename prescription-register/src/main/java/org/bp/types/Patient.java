package org.bp.types;

public class Patient {

	protected String firstName;
	protected String lastName;
	protected String gender;
	protected int age;
	
	
	public boolean isWrong() {
			
			if(this.getFirstName() 	!= null && this.getFirstName() 	!= "" &&
			   this.getLastName() 	!= null && this.getLastName() 	!= "" &&
			   this.getGender() 	!= null && this.getGender() 	!= "" ){
			
				return false;
			}
			else
				return true;
				
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}

}
