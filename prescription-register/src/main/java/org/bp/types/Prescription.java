package org.bp.types;

import java.util.ArrayList;

public class Prescription {

	protected int id;
	protected Patient patient;
	protected ArrayList<Drug> drugs;
	protected String extraDescription;
	
	public Prescription(int id, Patient patient, ArrayList<Drug> drugs, String extraDescription) {
		this.id = id;
		this.patient = patient;
		this.drugs = drugs;
		this.extraDescription = extraDescription;
	}
	

	
	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public Patient getPatient() {
		return patient;
	}



	public void setPatient(Patient patient) {
		this.patient = patient;
	}



	public Patient getPreson() {
		return patient;
	}
	public void setPreson(Patient preson) {
		this.patient = preson;
	}
	public ArrayList<Drug> getDrugs() {
		if(this.drugs == null) {
			this.drugs = new ArrayList<Drug>();
		}
		return drugs;
	}
	public void setDrugs(ArrayList<Drug> drugs) {
		this.drugs = drugs;
	}
	public String getExtraDescription() {
		return extraDescription;
	}
	public void setExtraDescription(String extraDescription) {
		this.extraDescription = extraDescription;
	}
	
	
}
