package org.bp.types;

public class Drug {

	protected String name;
	protected int quantity;
	protected String dosage;
	
	public boolean isWrong() {
		if(this.name   	 == null || this.name 	== "" ||
		   this.dosage 	 == null || this.dosage	== "" ||
		   this.quantity <= 0){
				return true;
			}
		else {
			return false;
		}
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getDosage() {
		return dosage;
	}
	public void setDosage(String dosage) {
		this.dosage = dosage;
	}
	
	
}
